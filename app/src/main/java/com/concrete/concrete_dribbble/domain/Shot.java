package com.concrete.concrete_dribbble.domain;

import org.androidannotations.annotations.EBean;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;
import java.util.List;

@EBean
public class Shot {

    @JsonProperty("id")
    Long id;
    @JsonProperty("title")
    String title;
    @JsonProperty("description")
    String description;
    @JsonProperty("width")
    Integer width;
    @JsonProperty("height")
    Integer height;
    @JsonProperty("rebound_source_id")
    Long rebound_source_id;
    @JsonProperty("views_count")
    Integer views_count;
    @JsonProperty("likes_count")
    Integer likes_count;
    @JsonProperty("comments_count")
    Integer comments_count;
    @JsonProperty("attachments_count")
    Integer attachments_count;
    @JsonProperty("rebounds_count")
    Integer rebounds_count;
    @JsonProperty("buckets_count")
    Integer buckets_count;
    @JsonProperty("created_at")
    String created_at;
    @JsonProperty("update_at")
    String updated_at;
    @JsonProperty("url")
    String url;
    @JsonProperty("short_url")
    String short_url;
    @JsonProperty("html_url")
    String html_url;
    @JsonProperty("attachments_url")
    String attachments_url;
    @JsonProperty("buckets_url")
    String buckets_url;
    @JsonProperty("comments_url")
    String comments_url;
    @JsonProperty("likes_url")
    String likes_url;
    @JsonProperty("projects_url")
    String projects_url;
    @JsonProperty("rebounds_url")
    String rebounds_url;
    @JsonProperty("image_url")
    String image_url;
    @JsonProperty("image_teaser_url")
    String image_teaser_url;
    @JsonProperty("image_400_url")
    String image_400_url;
    @JsonProperty("tags")
    List<String> tags;
    @JsonProperty("player")
    Player player;

    public Long getId(){
        return id;
    }

    public String getDescription(){
        return description;
    }

    public String getImageUrl(){
        return image_url;
    }

    public String getImageTeaserUrl(){
        return image_teaser_url;
    }

    public String getImage400Url(){
        return image_400_url;
    }

    public Player getPlayer(){
        return player;
    }

}
