package com.concrete.concrete_dribbble.domain;

import org.androidannotations.annotations.EBean;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

@EBean
public class Player {

    @JsonProperty("id")
    Long id;
    @JsonProperty("name")
    String name;
    @JsonProperty("location")
    String location;
    @JsonProperty("followers_count")
    Integer followers_count;
    @JsonProperty("draftees_count")
    Integer draftees_count;
    @JsonProperty("likes_count")
    Integer likes_count;
    @JsonProperty("likes_received_count")
    Integer likes_received_count;
    @JsonProperty("comments_count")
    Integer comments_count;
    @JsonProperty("comments_received_count")
    Integer comments_received_count;
    @JsonProperty("rebounds_count")
    Integer rebounds_count;
    @JsonProperty("rebounds_received_count")
    Integer rebounds_received_count;
    @JsonProperty("url")
    String url;
    @JsonProperty("avatar_url")
    String avatar_url;
    @JsonProperty("username")
    String username;
    @JsonProperty("twitter_screen_name")
    String twitter_screen_name;
    @JsonProperty("website_url")
    String website_url;
    @JsonProperty("drafted_by_player_id")
    Integer drafted_by_player_id;
    @JsonProperty("shots_count")
    Integer shots_count;
    @JsonProperty("following_count")
    Integer following_count;
    @JsonProperty("created_at")
    String created_at;

    public String getAvatarUrl(){
        return avatar_url;
    }

    public String getName(){
        return name;
    }

}
