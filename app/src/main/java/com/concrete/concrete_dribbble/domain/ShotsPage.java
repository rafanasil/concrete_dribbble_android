package com.concrete.concrete_dribbble.domain;

import org.androidannotations.annotations.EBean;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@EBean
public class ShotsPage {

    @JsonProperty("page")
    String page;
    @JsonProperty("per_page")
    Integer per_page;
    @JsonProperty("pages")
    Integer pages;
    @JsonProperty("total")
    Integer total;
    @JsonProperty("shots")
    List<Shot> shots;

    public List<Shot> getShots() {
        return shots;
    }

    public String getPage(){
        return page;
    }

}
