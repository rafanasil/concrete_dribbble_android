package com.concrete.concrete_dribbble;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.concrete.concrete_dribbble.custom_adapter.PopularShotsArrayAdapter;
import com.concrete.concrete_dribbble.domain.Shot;
import com.concrete.concrete_dribbble.domain.ShotsPage;
import com.concrete.concrete_dribbble.helper.NetworkHelper;
import com.concrete.concrete_dribbble.service.rest.ShotsRestClient;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

@NoTitle
@Fullscreen
@EActivity(R.layout.activity_detail)
public class DetailActivity extends Activity {

    @Extra("shot_id")
    Long mShotId;

    @RestService
    ShotsRestClient mShotsRestClient;

    @ViewById(R.id.imgShotDetail)
    ImageView imgShotDetail;

    @ViewById(R.id.imgAuthor)
    ImageView imgAuthor;

    @ViewById(R.id.txtAuthorName)
    TextView txtAuthorName;

    @ViewById(R.id.txtShotDescription)
    TextView txtShotDescription;

    @ViewById(R.id.txtSemInternetDetail)
    TextView txtSemInternet;

    @ViewById(R.id.btnTentarInternetDetail)
    Button btnTentarInternet;

    @AfterViews
    void afterViews() {

        if (NetworkHelper.isOnline(this)) {
            getShotDetails(mShotId);
        }
        else{
            imgShotDetail.setVisibility(View.GONE);
            imgAuthor.setVisibility(View.GONE);
            txtAuthorName.setVisibility(View.GONE);
            txtShotDescription.setVisibility(View.GONE);
            txtSemInternet.setVisibility(View.VISIBLE);
            btnTentarInternet.setVisibility(View.VISIBLE);
        }

    }

    @Background
    void getShotDetails(Long id){
        Shot shot = mShotsRestClient.getShot(id);
        showShotDetails(shot);
    }

    @UiThread
    void showShotDetails(Shot shot){
        Picasso.with(this)
                .load(shot.getImageUrl())
                .into((ImageView) imgShotDetail);

        Picasso.with(this)
                .load(shot.getPlayer().getAvatarUrl())
                .into((ImageView) imgAuthor);

        txtAuthorName.setText(shot.getPlayer().getName());

        if (shot.getDescription() != null)
            txtShotDescription.setText(Html.fromHtml(shot.getDescription()));
        else
            txtShotDescription.setText(shot.getDescription());
    }

    @Click
    void btnTentarInternetDetail() {
        if (NetworkHelper.isOnline(this)) {
            getShotDetails(mShotId);

            imgShotDetail.setVisibility(View.VISIBLE);
            imgAuthor.setVisibility(View.VISIBLE);
            txtAuthorName.setVisibility(View.VISIBLE);
            txtShotDescription.setVisibility(View.VISIBLE);
            txtSemInternet.setVisibility(View.GONE);
            btnTentarInternet.setVisibility(View.GONE);
        }
        else{
            imgShotDetail.setVisibility(View.GONE);
            imgAuthor.setVisibility(View.GONE);
            txtAuthorName.setVisibility(View.GONE);
            txtShotDescription.setVisibility(View.GONE);
            txtSemInternet.setVisibility(View.VISIBLE);
            btnTentarInternet.setVisibility(View.VISIBLE);
        }
    }

}
