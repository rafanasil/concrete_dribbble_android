package com.concrete.concrete_dribbble.service.rest;

import com.concrete.concrete_dribbble.domain.Shot;
import com.concrete.concrete_dribbble.domain.ShotsPage;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

@Rest(rootUrl = "http://api.dribbble.com/shots", converters = { MappingJacksonHttpMessageConverter.class })
public interface ShotsRestClient {
    @Get("/popular?page={page}")
    ShotsPage getPopularShots(int page);

    @Get("/{id}")
    Shot getShot(Long id);
}