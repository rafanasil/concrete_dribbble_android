package com.concrete.concrete_dribbble;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.concrete.concrete_dribbble.custom_adapter.PopularShotsArrayAdapter;
import com.concrete.concrete_dribbble.custom_listener.EndlessScrollListener;
import com.concrete.concrete_dribbble.domain.Shot;
import com.concrete.concrete_dribbble.domain.ShotsPage;
import com.concrete.concrete_dribbble.helper.NetworkHelper;
import com.concrete.concrete_dribbble.service.rest.ShotsRestClient;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

@NoTitle
@Fullscreen
@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {

    @ViewById(R.id.lvwPopularShots)
    ListView lvwPopularShots;

    @ViewById(R.id.txtSemInternet)
    TextView txtSemInternet;

    @ViewById(R.id.btnTentarInternet)
    Button btnTentarInternet;

    @RestService
    ShotsRestClient mShotsRestClient;

    PopularShotsArrayAdapter mPopularShotsArrayAdapter;
    SwipeRefreshLayout mSwipeLayout;

    @AfterViews
    void afterViews() {
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        if (NetworkHelper.isOnline(this)) {
            mSwipeLayout.setRefreshing(true);
            getPopularShots(1);
        }
        else{
            mSwipeLayout.setVisibility(View.GONE);
            txtSemInternet.setVisibility(View.VISIBLE);
            btnTentarInternet.setVisibility(View.VISIBLE);
        }

    }

    @Background
    void getPopularShots(int page){
        ShotsPage popularShotsPage = mShotsRestClient.getPopularShots(page);
        listPopularShots(popularShotsPage);
    }

    @UiThread
    void listPopularShots(ShotsPage popularShotsPage){
        if (popularShotsPage.getPage().equals("1")) {
            mPopularShotsArrayAdapter = new PopularShotsArrayAdapter(this, popularShotsPage.getShots());
            lvwPopularShots.setAdapter(mPopularShotsArrayAdapter);

            lvwPopularShots.setOnScrollListener(new EndlessScrollListener() {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    if (NetworkHelper.isOnline(getApplicationContext())) {
                        mSwipeLayout.setRefreshing(true);
                        getPopularShots(page);
                    }
                    else{
                        mSwipeLayout.setVisibility(View.GONE);
                        txtSemInternet.setVisibility(View.VISIBLE);
                        btnTentarInternet.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
        else {
            mPopularShotsArrayAdapter.addAll(popularShotsPage.getShots());
            mPopularShotsArrayAdapter.notifyDataSetChanged();
        }
        mSwipeLayout.setRefreshing(false);
    }

    @ItemClick(R.id.lvwPopularShots)
    void popularShotsListItemClicked(Shot selectedShot) {
        DetailActivity_.intent(this).mShotId(selectedShot.getId()).start();
    }

    @Click
    void btnTentarInternet() {
        if (NetworkHelper.isOnline(this)) {
            mSwipeLayout.setRefreshing(true);
            getPopularShots(1);

            mSwipeLayout.setVisibility(View.VISIBLE);
            txtSemInternet.setVisibility(View.GONE);
            btnTentarInternet.setVisibility(View.GONE);
        }
        else{
            mSwipeLayout.setVisibility(View.GONE);
            txtSemInternet.setVisibility(View.VISIBLE);
            btnTentarInternet.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        if (NetworkHelper.isOnline(this)) {
            mSwipeLayout.setRefreshing(true);
            getPopularShots(1);

            mSwipeLayout.setVisibility(View.VISIBLE);
            txtSemInternet.setVisibility(View.GONE);
            btnTentarInternet.setVisibility(View.GONE);
        }
        else{
            mSwipeLayout.setVisibility(View.GONE);
            txtSemInternet.setVisibility(View.VISIBLE);
            btnTentarInternet.setVisibility(View.VISIBLE);
        }
    }

}
