package com.concrete.concrete_dribbble.custom_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.concrete.concrete_dribbble.R;
import com.concrete.concrete_dribbble.domain.Shot;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PopularShotsArrayAdapter extends ArrayAdapter<Shot> {
    private final Context context;
    private final List<Shot> shots;

    public PopularShotsArrayAdapter(Context context, List<Shot> shots) {
        super(context, R.layout.list_item_popular_shots, shots);
        this.context = context;
        this.shots = shots;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_popular_shots, parent, false);

        ImageView imgShot = (ImageView) rowView.findViewById(R.id.imgShot);
        Shot shot = this.shots.get(position);

        Picasso.with(this.context)
                .load(shot.getImage400Url())
                .into((ImageView) imgShot);

        return rowView;
    }
}